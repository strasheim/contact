#!/bin/bash

CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-s -w -extldflags \"-static\"' && \
    upx -q --ultra-brute contact && \
    zip contact.zip contact && \
    aws lambda update-function-code --function-name GoTo --zip-file fileb://contact.zip --publish

