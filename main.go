package main

import (
	"context"
	"errors"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/aws/aws-sdk-go/aws/arn"
)

func handler(ctx context.Context, snsEvent events.SNSEvent) {

	origin, err := locate(ctx)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(origin.AccountID, origin.Region)
}
func main() {
	lambda.Start(handler)
}

func locate(ctx context.Context) (myOwn arn.ARN, err error) {
	stuff, okay := lambdacontext.FromContext(ctx)
	if okay == false {
		err = errors.New("Failed to read Lambda Context")
		return
	}
	myOwn, err = arn.Parse(stuff.InvokedFunctionArn)
	if len(myOwn.AccountID) == 0 {
		err = errors.New("Account ID too short")
	}
	return
}
