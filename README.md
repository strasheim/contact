
#### Building the binary 

- Builing a stripped static binary (or unstripped)
- Compressing the binary (or not)
- Zipping it up 
- Uploading the zipball

```
CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-s -w -extldflags \"-static\"' && \
    upx -q --ultra-brute contact && \
    zip contact.zip contact && \
    aws lambda update-function-code --function-name GoTo --zip-file fileb://contact.zip --publish
```

*versus*

```
GOOS=linux go build  && \
    zip contact.zip contact && \
    aws lambda update-function-code --function-name GoTo --zip-file fileb://contact.zip --publish
```


This works if the function already exists is named GoTo and the hander is set to contact. 

#### What does this do? 
A very tiny function to play with AWS Lambda settings. This one just tries to find out where it is and what account it's been running on. The compression was to get some benchmarking out of lambda functions. 

- Lambdas are loaded (as it looks) on workers
- Load time is about ~ 16ms in eu-central-1 
- If executed a lot the load time goes away 
- Compression at the size in this example does not matter to much. 
- Size difference is 1.4MB to 3MB for the upload 
 
Unzip time: (full binary) 
```
time unzip contact.zip 
Archive:  contact.zip
  inflating: contact                 

real	0m0.105s
user	0m0.080s
sys	    0m0.017s
```

The brute compressed binary
```
time unzip contact.zip 
Archive:  contact.zip
  inflating: contact                 

real	0m0.017s
user	0m0.011s
sys	    0m0.005s
```

*BOOM* Headshot - zip on the second will not spend as much time and it's faster to be moved around as the zip is only half the size to begin with. The unzip and the copy will hit you on every cold start.

However you require more CPU power to decompress every run cause the binary is packed. For this I run in local docker lambda container the execution in a loop. This means you will be slower and chances are a lot when you run the binary a lot. If your function is called only every few hours and you always look at a cold start ... your function will be faster with the strong compression. 


In between worlds - stripped binary without compression and zip with -0 (no compression) uncompressed
```
time unzip contact.zip 
Archive:  contact.zip
 extracting: contact                 

real	0m0.051s
user	0m0.034s
sys	    0m0.012s
```
This makes a nice middle ground - the time is higher cause files size is 5.3MB vs the 1.4MB of the compressed binary. Uploading the file will take longer and AWS will need to copy more data around on - yet given their normal sizing I think it's save to assume 1GB links and SSDs for where things are stored. Transfer rate would be (technically) 119.21 MB/sec ~ 

- 1.4 mb ~ 0.011s
- 3.0 mb ~ 0.025s
- 5.3 mb ~ 0.044s 

Without the decompression at runtime copy + decompression (coldstart) will be:
- Strong Compressed:   0.028s
- AWS example default: 0.130s 
- Zip as container:    0.095s 

The AWS example default is the slowest and there is no good reason to do so unless your upload to lambda itself is important. 
The strong compressed while fastest in the list, does have the extra time to start caues of it's onthefly decompression 
The Zip as container option saves you some time and generally does not have downsides (beside zip size).

It's hard to say what you are losing to on the fly decompression. A hello world go program run locally: 

- Size uncompressed 2.1MB
- Size compressed 0.59MB
- Speed: uncom: 0.008s
- Speed: comp.: 0.055s

Yet those 47ms would still be faster on AWS. There are no small golang programs as lambda function out there if you use other AWS resources and those libs are part of the upload. Uncompressed you are normally at 10mb up to 15mb. 

All this is only important if your function isn't running concurrently. If the lambda is loaded already on the container you have little need to compress it. 

--- 
If you wish more details on Lambda Cold starts there is a [very nice blog](https://hackernoon.com/im-afraid-you-re-thinking-about-aws-lambda-cold-starts-all-wrong-7d907f278a4f) about it. 

More [data points](https://www.iopipe.com/2016/09/understanding-aws-lambda-coldstarts/) 


##### AWS account - 

Yes the example assumes you have an AWS account - you can play with this in a free tier.


##### Lamdba testing - 

A very awesome docker framework was put togehter by a few hackers out there, which clones the lambda world. It's super simple to run and test with. This does not help you at all if you integrate with other AWS components, but generally gives you a good idea on how it's actually done under the hood. 

* [Overview](https://github.com/lambci/docker-lambda)
* How to run on this: docker run --rm -v "$PWD":/var/task lambci/lambda:go1.x contact '{"some": "event"}'
